package org.smartboot.aio;

import java.nio.ByteBuffer;


/**
 * @author 三刀
 */
final class ByteBufferArray {
    private final ByteBuffer[] buffers;
    private final int offset;
    private final int length;

    public ByteBufferArray(ByteBuffer[] buffers, int offset, int length) {
        this.buffers = buffers;
        this.offset = offset;
        this.length = length;
    }

    public ByteBuffer[] getBuffers() {
        return buffers;
    }

    public int getOffset() {
        return offset;
    }

    public int getLength() {
        return length;
    }
}